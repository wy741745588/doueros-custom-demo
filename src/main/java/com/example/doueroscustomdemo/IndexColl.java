package com.example.doueroscustomdemo;


import com.baidu.dueros.samples.tax.TaxBot;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class IndexColl{
    @RequestMapping(value = "/test" ,method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public String Test(@RequestBody String json) {
        try {
            System.out.println(json);
            TaxBot bot = new TaxBot(json);
            bot.disableVerify();
            String responseJson = bot.run();
            return responseJson;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}


