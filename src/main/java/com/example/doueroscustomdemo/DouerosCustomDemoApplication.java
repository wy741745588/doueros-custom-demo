package com.example.doueroscustomdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DouerosCustomDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DouerosCustomDemoApplication.class, args);

    }

}
