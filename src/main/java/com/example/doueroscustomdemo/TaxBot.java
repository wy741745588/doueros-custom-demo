package com.example.doueroscustomdemo;

import com.baidu.dueros.bot.BaseBot;
import com.baidu.dueros.data.request.IntentRequest;
import com.baidu.dueros.data.request.LaunchRequest;
import com.baidu.dueros.data.request.SessionEndedRequest;
import com.baidu.dueros.data.response.OutputSpeech;
import com.baidu.dueros.data.response.card.TextCard;
import com.baidu.dueros.model.Response;

import java.io.IOException;

public class TaxBot extends BaseBot {


    protected TaxBot(String request) throws IOException {
        super(request);
    }

    @Override
    protected Response onLaunch(LaunchRequest launchRequest) {
        System.out.println("in onlaunch");
        // 新建文本卡片
        TextCard textCard = new TextCard("所得税为您服务");
        // 设置链接地址
        textCard.setUrl("www:....");
        // 设置链接内容
        textCard.setAnchorText("setAnchorText");
        // 添加引导话术
        textCard.addCueWord("欢迎进入");
        // 新建返回的语音内容
        OutputSpeech outputSpeech = new OutputSpeech(OutputSpeech.SpeechType.PlainText, "所得税为您服务");
        // 构造返回的Response
        Response response = new Response(outputSpeech, textCard);
        return response;
    }
    @Override
    protected Response onSessionEnded(SessionEndedRequest sessionEndedRequest) {
        System.out.println("in onSessionEnded");
        // 构造TextCard
        TextCard textCard = new TextCard("感谢使用所得税服务");
        textCard.setAnchorText("setAnchorText");
        textCard.addCueWord("欢迎再次使用");

        // 构造OutputSpeech
        OutputSpeech outputSpeech = new OutputSpeech(OutputSpeech.SpeechType.PlainText, "欢迎再次使用所得税服务");

        // 构造Response
        Response response = new Response(outputSpeech, textCard);

        return response;
    }
    @Override
    protected Response onInent(IntentRequest intentRequest) {
        // 判断NLU解析的意图名称是否匹配
        System.out.println(intentRequest.getIntentName());
        return new Response();
    }

}
